import React from "react";
import Button from "@material-ui/core/Button";

export const WarmUpFrontEnd = props => {
  const exampleData = [
    { name: "rick", cars: ["Corvette Z06", "Lotus Exite S"] },
    { name: "john", cars: ["BMW 320D"] },
    { name: "zing", cars: ["Honda Jazz", "Honda Click", "Honda Waves"] }
  ];
  function mapCars(newData) {
    const cars = newData;
    return cars.map((newCars, index) => {
      const evenIndexOfCarAndlengthArrayTwo = index % 2 && cars.length === 2;
      if (evenIndexOfCarAndlengthArrayTwo) {
        return `,${newCars}`;
      } else if (evenIndexOfCarAndlengthArrayTwo === false) {
        return `,${newCars},`;
      } else return newCars;
    });
  }
  function sortName(params) {
    const afterSort = exampleData.sort((firstDate, secondData) =>
      firstDate.name > secondData.name ? 1 : -1
    );
    return afterSort.map((newData, index) => (
      <div style={{ flexDirection: "row" }}>
        <p>
          {newData.name.slice(0, 1).toUpperCase() + newData.name.slice(1)} want
          to buy {mapCars(newData.cars)}
        </p>
      </div>
    ));
  }
  return (
    <div>
      {sortName()}
      <div style={{ margin: 20 }}>
        <Button
          variant="contained"
          onClick={() => {
            props.history.push("/todoapp");
          }}
        >
          Next answer
        </Button>
      </div>
    </div>
  );
};
