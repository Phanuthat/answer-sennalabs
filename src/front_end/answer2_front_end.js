import React from "react";
export const FindUniq = () => {
  const exampleData = [1, 1, 1, 2, 1, 1];
  const exampleDataTwo = [0, 0, 0.55, 0, 0];
  const arrayUnique = props => {
    return props.filter((item, index) => {
      return props.indexOf(item) !== 0 ? item : "";
    });
  };
  return (
    <div>
      <div>{arrayUnique(exampleDataTwo)}</div>
      <div>{arrayUnique(exampleData)}</div>
    </div>
  );
};
